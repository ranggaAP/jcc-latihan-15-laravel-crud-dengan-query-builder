<?php

namespace App\Http\Controllers;

use App\Models\Cast;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::get();

        $data = [
            'casts' => $casts
        ];

        return view('cast.index', $data);
    }

    public function destroy($id)
    {
        $row = Cast::where('id', $id)
            ->first();

        if($row) {
            Cast::where('id', $id)
                ->delete();

            return Redirect::back();
        } else {
            abort(404);
        }
    }

    public function edit($id)
    {
        $row = Cast::where('id', $id)
            ->first();

        if($row) {
            $data   = [
                'id'    => $id,
                'row'   => $row
            ];

            return view('cast.edit', $data);
        } else {
            abort(404);
        }
    }

    public function create()
    {
        return view('cast.create');
    }

    public function show($id)
    {
        $row = Cast::where('id', $id)
            ->first();

        if($row) {
            $data   = [
                'row' => $row
            ];

            return view('cast.detail', $data);
        } else {
            abort(404);
        }
    }

    public function store(Request $request)
    {
        try {
            $data   = [
                'bio'   => $request->bio,
                'nama'  => $request->nama,
                'umur'  => $request->umur,
            ];

            $cast = Cast::create($data);

            if($cast) {
                return Redirect::to('/casts');
            }
        } catch (Exception $e) {
            dd($e);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $data   = [
                'bio'   => $request->bio,
                'nama'  => $request->nama,
                'umur'  => $request->umur,
            ];

            $cast = Cast::where('id', $id)
                ->update($data);

            if($cast) {
                return Redirect::to('/casts');
            }
        } catch (Exception $e) {
            dd($e);
        }
    }
}
