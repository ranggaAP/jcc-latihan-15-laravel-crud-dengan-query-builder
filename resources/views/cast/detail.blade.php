<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cast</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container" style="margin-top: 50px;">
        <a href="{{ URL::to('/casts') }}" class="btn btn-primary btn-xs">
            Kembali
         </a>
        <h4>Keterangan Cast</h4>
        <div class="card">
            <div class="card-body">
                <ul>
                    <li>Nama : {{ $row->nama }}</li>
                    <li>Umur : {{ $row->umur }} Tahun</li>
                    <li>Bio : {{ $row->bio }}</li>
                </ul>
            </div>
        </div>
    </div>
</body>
</html>