<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cast</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container" style="margin-top: 50px;">
        <div class="row">
           <a href="{{ URL::to('/casts/create') }}" class="btn btn-primary btn-xs">
               <span class="glyphicon glyphicon-plus"></span> Tambah data
            </a>
            <div class="table-responsive">
                <table class="table table-bordred table-striped table-hover">
                    <thead>
                        <th>#</th>
                        <th width="80%">Nama</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        @foreach($casts as $cast)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $cast->nama }}</td>
                                <td>
                                    <a href="{{ URL::to('/cast/'.$cast->id) }}" class="btn btn-info btn-xs" data-placement="top" data-toggle="tooltip" title="Keterangan" >
                                        <span class="glyphicon glyphicon-eye-open"></span>
                                    </a>
                                    <a href="{{ URL::to('/cast/'.$cast->id.'/edit') }}" class="btn btn-primary btn-xs" data-placement="top" data-toggle="tooltip" title="Edit" >
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <form action="{{ URL::to('/cast/'.$cast->id) }}" method="POST" style="display: inline-block">
                                        <button class="btn btn-danger btn-xs" data-placement="top" data-toggle="tooltip" title="Hapus">
                                            <span class="glyphicon glyphicon-trash"></span>
                                        </button>
                                        {!! method_field('delete') !!}
                                        {!! csrf_field() !!}
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>