<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cast</title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
        <form action="{{ URL::to('/cast/'.$id) }}" method="POST">
            @csrf
            @method('PUT')
            <h3>Edit Cast</h3>
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" id="nama" name="nama" class="form-control" value="{{ $row->nama }}" required>
            </div>
            <div class="form-group">
                <label for="umur">Umur</label>
                <input type="number" id="umur" name="umur" value="{{ $row->umur }}" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="bio">Bio</label>
                <textarea name="bio" id="bio" class="form-control" required>{{ $row->bio }}</textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-primary btn-xs">
                    Update data
                 </button>
            </div>
        </form>
    </div>
</body>
</html>