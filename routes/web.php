<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/casts', [CastController::class, 'index']);
Route::get('/casts/create', [CastController::class, 'create']);
Route::get('/cast/{id}', [CastController::class, 'show']);
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::post('/cast', [CastController::class, 'store']);
Route::put('/cast/{id}', [CastController::class, 'update']);  
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
